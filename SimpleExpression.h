#pragma once

#include <iostream>
#include <vector>

using namespace std;


class SimpleExpression
{
	enum
	{
		NM_ADD = 0,
		NM_SUB,
		NM_MUL,
		NM_DIV,
		NM_OPERAND,
		NM_SUBEXPRESSION,
		NM_EOS,
		NM_ERROR,
		NM_UNDEFINED = 0xFF,
	};

public:
	SimpleExpression(const string& expression, const unsigned int size);
	~SimpleExpression();
	int calculate(double& result);

private:
	bool isStringValid();
	int getSubexprSize(const char *first_chr, const char *last_chr, unsigned int *size);
	uint16_t getNextMember(const char *first_chr, const char *last_chr);	

	string expr;
};

