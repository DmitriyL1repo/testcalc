#include "stdafx.h"
#include <algorithm>
#include "SimpleExpression.h"


//-----------------------------------------------------------------------------------


SimpleExpression::SimpleExpression(const string& expression, const unsigned int size)
{	
	expr.clear();	
	expr.append(expression.data(), size);
}

//-----------------------------------------------------------------------------------

SimpleExpression::~SimpleExpression()
{
}

//-----------------------------------------------------------------------------------

bool SimpleExpression::isStringValid(void)
{
	if (expr.size() == 0)
		return false;
	
	for (unsigned int i = 0; i < expr.size(); i++)
	{
		if (!isdigit(expr[i]) && !(expr[i] == '+' || expr[i] == '-' || expr[i] == '*' ||
			expr[i] == '/' || expr[i] == '.' || expr[i] == '(' || expr[i] == ')' || expr[i] == ' '))
			return false;

	}
	return true;
}

//-----------------------------------------------------------------------------------
//Retval
// 0 - OK
// 1 - wrong input parameter
// 2 - parse error

int SimpleExpression::getSubexprSize(const char *first_chr, const char *last_chr, unsigned int *size)
{
	int intrn_brackets_qty = 0;
	int i;
	const char *p = first_chr;

	if (first_chr == nullptr || last_chr == nullptr || size == nullptr || (first_chr >= last_chr))
		return 1;

	for (i = 0; i < (last_chr - first_chr); i++)
	{
		if (p[i] == '(')
			intrn_brackets_qty++;
		if (p[i] == ')')
			intrn_brackets_qty--;
		if (intrn_brackets_qty == -1)
		{
			*size = i;
			return 0;
		}
	}
	return 2;
}

//-----------------------------------------------------------------------------------

uint16_t SimpleExpression::getNextMember(const char *first_chr, const char *last_chr)
{
	const char *p = first_chr;
	uint16_t next_member = NM_UNDEFINED;

	if (first_chr == nullptr || last_chr == nullptr || (first_chr >= last_chr))
		return NM_ERROR;

	while (p <= last_chr)
	{
		if (*p == '-')		
			next_member = NM_SUB;					
		else if (*p == '+')
			next_member = NM_ADD;
		else if (*p == '*')
			next_member = NM_MUL;
		else if (*p == '/')
			next_member = NM_DIV;
		else if (*p == '(')
			next_member = NM_SUBEXPRESSION;
		else if (isdigit(*p))
			next_member = NM_OPERAND;

		if (next_member != NM_UNDEFINED)
		{			
			first_chr = p;
			return next_member;
		}
		p++;
	}

	return NM_EOS;
}

//-----------------------------------------------------------------------------------
//Retval
// 0 - OK
// 1 - parse error

int SimpleExpression::calculate(double& result)
{	
	const char *p, *p_end;
	vector<double> operands_1, operands_2;
	vector<uint16_t> operators_1, operators_2;
	uint16_t next_member;
	double result_tmp = 0.0;
		
	if (!isStringValid()) 
		return 1;

	p = expr.data();
	p_end = p + expr.size();
	
	//Parse input data
	for ( ; p < p_end; )
	{
		next_member = getNextMember(p, p_end);
		if (next_member == NM_OPERAND)
			operands_1.push_back(strtod(p, (char**)&p));
		else if (next_member < NM_OPERAND)
		{
			operators_1.push_back(next_member);
			p++;
		}
		else if (next_member == NM_SUBEXPRESSION)
		{
			double temp_res;
			unsigned int subexpr_sz;
			
			++p;
			if (getSubexprSize(p, p_end, &subexpr_sz))
				break;

			SimpleExpression subexpr(p, subexpr_sz);
			if (subexpr.calculate(temp_res))
				return 1;

			operands_1.push_back(temp_res);
			p += subexpr_sz + 1;
		}
		else
			break;
	}
	
	if ((operands_1.size() < 2) || (operands_1.size() - 1) != operators_1.size())
		return 1;
	
	//Do calculation for high priority operators ( div and mul )
	bool prev_op_flag = false;
	for (size_t i = 0; i < operands_1.size(); i++)
	{		
		if (i == operators_1.size())
			break;

		if (operators_1[i] == NM_MUL)
		{
			if (prev_op_flag)				
				operands_2.back() *= operands_1[i + 1];
			else
				operands_2.push_back(operands_1[i] * operands_1[i + 1]);

			prev_op_flag = true;			
		}
		else if (operators_1[i] == NM_DIV)
		{			
			if (prev_op_flag)
				operands_2.back() /= operands_1[i + 1];
			else
				operands_2.push_back(operands_1[i] / operands_1[i + 1]);

			prev_op_flag = true;			
		}
		else 
		{
			if (prev_op_flag == false)
				operands_2.push_back(operands_1[i]);

			if (i == (operands_1.size() - 2))
			{
				i++;
				operands_2.push_back(operands_1[i]);
			}
			prev_op_flag = false;
		}
	}

	//Do calculation for the rest of operators
	result_tmp = operands_2.front();
	for (size_t i = 1, k = 0; i < operands_2.size(); i++, k++)
	{
		while (operators_1[k] != NM_ADD && operators_1[k] != NM_SUB)
			k++;
		
		if (operators_1[k] == NM_ADD)
			result_tmp += operands_2[i];
		else if (operators_1[k] == NM_SUB)
			result_tmp -= operands_2[i];
	}

	
	//-------- TEST SECTION -------
	/*
	cout << "operators:" << endl;	
	for each (size_t i in operators_1)
	{
		cout << operators_1[i] << ' ';
	}
	
	cout << endl<< "operands:" << endl;
	for each (size_t i in operands)
	{
		cout << operands[i] << ' ';
	}
	*/	
	//cout << endl;	
	//----- END OF TEST SECTION ----	

	result = result_tmp;
	return 0;
}





