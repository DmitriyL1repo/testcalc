// TestCalc.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include "SimpleExpression.h"

using namespace std;


int main()
{
	string input_string;	
	char scancode = 0;
	double result;
	int retval;

	cout << "Type expression and press Enter" << endl;			
	//cin >> input_string;	//This construction truncate string to the first SPACE occured.
	while (scancode != '\r')
	{		
		scancode = _getch();
		
		if (scancode > 0 && scancode != ' ' && isprint(scancode)) //prefiltering input data
		{
			cout << scancode;
			input_string.push_back(scancode);
		}			
	}
	cout << endl;
	
	SimpleExpression expression(input_string, input_string.size());

	retval = expression.calculate(result);

	if (retval)
		cout << "Calculation error: " << retval << endl;
	else
		cout << "Calculation result: " << result << endl;

	cout << "Press any key to exit" << endl;
	_getch();
	
	return 0;
}

